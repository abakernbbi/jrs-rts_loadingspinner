import React, { useState} from 'react';

import LoadingSpinner from './components/LoadingSpinner/LoadingSpinner';




const App: React.FC = () => {
  let [loading, setLoading] = useState(false);

    return (
      <div>
        <button onClick={() => setLoading(!loading)}>Toggle Loading</button>
        <p>{loading.toString()}</p>
        <LoadingSpinner toggle={loading}></LoadingSpinner>
      </div>
    )
  }

export default App;
