import './LoadingSpinner.css'

export interface LoadingSpinnerProps {
  toggle: boolean;
}




const LoadingSpinner: React.FC<LoadingSpinnerProps> = ({toggle}) => {
 return (
   <div>
  { 
  toggle && 
    <div className="LoadingSpinner__Container">
      <div className="LoadingSpinner"></div> 
      <p><i>Loading...</i></p>
    </div>
  }
  </div>
 );
}

export default LoadingSpinner